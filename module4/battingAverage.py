import re
import math
import collections
import sys, os
import operator
#Search around the key words 'batted' and 'times'
#group results into 3 groups, Name, bats, hits
batting_regex =re.compile(r"([\w]+\s+[\w]+)\s+batted+\s+(\d)+\stimes\swith\s(\d)")

#check to see if number of arguments is correct
if len(sys.argv) < 2:
	sys.exit("Usage: %s must be run with the correct path to an input file" % sys.argv[0])
if len(sys.argv) > 2:
	sys.exit("Usage: %s can only be run with one argument" % sys.argv[0])
#set second argument to be a file passed in
filename = sys.argv[1]
 
#print error message if file cannot be found
if not os.path.exists(filename):
    sys.exit("Error: File '%s' not found" % sys.argv[1])
    
    
#check to see if the player already exists or not.
#Return None if player is not in 'names' list
#else return some value
def check_null(name):
    for x in stats:
        z = x['name']
        y=name
        #print z+" "+y
        if z==y:
            #print z
            return z
    
def check_name(name,bats,hits):
    
    player = check_null(name)
       
    if player is None:        
        
        new_players ={'name': name, 'bats': bats, 'hits': hits}
        #print new_players
        #print type (bats)
        stats.append(new_players)
        #print stats
            
    else:     
       
       old_bats=get_bats(player)
       old_hits=get_hits(player)
       
       new_bats=old_bats+bats
       new_hits = old_hits+hits
       replace_stats(player,new_bats,new_hits)
       
def get_hits(name):
    for x in stats:
        if name==x['name']:
            y= x['hits']
            return y              
def get_bats(name):
    for x in stats:
        if name==x['name']:
            y= x['bats']
            return y
        
def replace_stats(name,bats,hits):
    for x in stats:
        if name==x['name']:
            x['bats'] = bats
            x['hits'] = hits
    
def math(name):
    hits = get_hits(name)
    bats = get_bats(name)
    avg = float(hits)/(bats)
    return avg
f=open(filename)

stats=[]
for line in f:
    # print "Read line: %s" % line.rstrip()
    match =re.match(batting_regex,line)
    if match:
            name= match.group(1)
            bats = int (match.group(2))
            hits = int (match.group(3))
            #print name
           # print bats
            
            check_name(name,bats,hits)
results=[]            
for x in stats:
	temp=[]
	
	name = x['name']    
	hits = x['hits']    
	bats = x['bats']   
	avg = float(hits)/(bats)
	
	#decimal = "{0:.12f}".format(avg)
	decimal = round(avg,12)
	temp = [name, decimal]
	
	results.append(temp)
	#print results
	
	# rounded = '{:.3f}'.format(avg)
	
	#print name+": ", rounded
f.close()

for x in results:
	n = x[1]
	rounded = "{0:.3f}".format(n)

results.sort(reverse=True)
for x in results:
	
	print x[0] + ": " + str(x[1])
	#print round(x[1], 3)
	
	#print '%.3f' % round(x[1], 3)
	
#print("\n".join(map(str, results)))
#print results